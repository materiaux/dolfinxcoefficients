# dolfinxcoefficients, a framework agnostic dolfinx extension for externally evaluated form coefficients 

`dolfinxcoefficients` comes with a C++ and a Python package, whereas the latter depends on the former. 
For details on each package visit the respective directories. This project is part of [`Materiaux`](https://gitlab.com/materiaux).


## Warning

This project is developed against the currently unreleased FEniCS-X ecosystem. It it thus not considered 
as 'stable' but highly experimental and given its early stage also 'incomplete'.

