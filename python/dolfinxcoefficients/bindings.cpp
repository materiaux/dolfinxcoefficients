// Copyright (C) 2019-2020 Matthias Rambausek
//
// This file is part of 'materiaux' (https://gitlab.com/materiaux/dolfinxcoeffcients)
//
// SPDX-License-Identifier:    AGPL-3.0-or-later


#include <functional>
#include <dolfinxcoefficients.h>
#include <pybind11/complex.h>
#include <pybind11/eigen.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

namespace py = pybind11;

namespace dolfinxcoefficients {

    PYBIND11_MODULE(dolfinxcoefficients_pybind, m) {
        m.doc() = "Support for local functions in form for FEniCS-X/dolfinx";

        py::class_<LocalFunction>(m, "LocalFunction")
            .def(py::init([](std::uintptr_t addr) {
                return LocalFunction{reinterpret_cast<local_function_t>(addr)};
            }))
            .def_readonly("function", &LocalFunction::function);

        py::class_<IntegralData, std::shared_ptr<IntegralData>>(m, "IntegralData")
            .def(py::init([](int id, dolfinx::fem::IntegralType type, std::uintptr_t addr,
                             std::vector<Coefficient> coefficients,
                             std::vector<Constant> constants,
                             bool needs_permutation_info) {
                return IntegralData{id, type, reinterpret_cast<tabulate_tensor_t>(addr),
                                    coefficients, constants, needs_permutation_info};
            }))
            .def_readonly("type", &IntegralData::type)
            .def_readonly("id", &IntegralData::id)
            .def_readonly("coefficients", &IntegralData::coefficients)
            .def_readonly("constants", &IntegralData::constants)
            .def_readonly("tabulate_tensor", &IntegralData::tabulate_tensor)
            .def_readonly("needs_permutation_data", &IntegralData::needs_permutation_data);

        py::class_<ExpressionData, std::shared_ptr<ExpressionData>>(m, "ExpressionData")
            .def(py::init([](std::uintptr_t addr, FiniteElement result,
                             std::vector<Coefficient> coefficients,
                             std::vector<Constant> constants) {
                return ExpressionData{reinterpret_cast<tabulate_expression_t>(addr),
                                      std::move(result), coefficients, constants};
            }))
            .def_readonly("element", &ExpressionData::element)
            .def_readonly("coefficients", &ExpressionData::coefficients)
            .def_readonly("constants", &ExpressionData::constants)
            .def_readonly("tabulate_expression", &ExpressionData::tabulate_expression);

        m.def("create_expression_data",
              [](std::uintptr_t addr_a, std::uintptr_t addr_L, FiniteElement result,
                 std::vector<Coefficient> coefficients, std::vector<Constant> constants) {
                  return create_expression_data(reinterpret_cast<tabulate_tensor_t>(addr_a),
                                                reinterpret_cast<tabulate_tensor_t>(addr_L),
                                                std::move(result), coefficients, constants);
              }
        );

        m.def("create_expression_data",
              py::overload_cast<LocalFunction, FiniteElement, std::vector<Coefficient>,
                                std::vector<Constant>>(&create_expression_data));

        m.def("create_form", create_form);
    }
} // namespace dolfinxcoefficients
