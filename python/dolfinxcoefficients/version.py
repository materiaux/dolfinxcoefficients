# Copyright (C) 2019-2020 Matthias Rambausek
#
# This file is part of 'materiaux' (https://gitlab.com/materiaux/dolfinxcoeffcients)
#
# SPDX-License-Identifier:    AGPL-3.0-or-later


__version__ = "0.1.0"


def check_compatibility(other_version: str):
    return __version__ == other_version
