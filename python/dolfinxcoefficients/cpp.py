# Copyright (C) 2019-2020 Matthias Rambausek
#
# This file is part of 'materiaux' (https://gitlab.com/materiaux/dolfinxcoeffcients)
#
# SPDX-License-Identifier:    AGPL-3.0-or-later

import typing
import numpy as np
import cffi
import ufl
import ufl.core.expr
from ufl.algorithms.analysis import extract_coefficients as ufl_extract_coefficients, \
    extract_constants as ufl_extract_constants
import dolfinx
import dolfinx.cpp.fem
import dolfinx.jit
import dolfinxcoefficients.dolfinxcoefficients_pybind as _bindings
from .dolfinxcoefficients_pybind import LocalFunction, ExpressionData, IntegralData


def _determine_quadrature_points_from_element(elmt: ufl.FiniteElementBase):
    if elmt.family() != "Quadrature":
        raise ValueError("Given element must belong to 'Quadrature' family of elements.")
    fe, dm = dolfinx.jit.ffcx_jit(elmt)
    ffi = cffi.FFI()
    fe = dolfinx.cpp.fem.FiniteElement(ffi.cast("uintptr_t", fe))
    return np.array(fe.dof_reference_coordinates()[::elmt.reference_value_size()])


def _determine_quadrature_points(cell_type: ufl.Cell, quadrature_degree: int,
                                 quadrature_rule: str = "default") -> np.ndarray:
    elmt = ufl.FiniteElement("Quadrature", cell_type, degree=quadrature_degree, quad_scheme=quadrature_rule)
    return _determine_quadrature_points_from_element(elmt)


def _extract_coefficients(item) -> typing.List[ufl.Coefficient]:
    try:
        return item.coefficients()
    except AttributeError:
        return ufl_extract_coefficients(item)


def _extract_constants(item) -> typing.List[ufl.Constant]:
    try:
        return item.constants()
    except AttributeError:
        return ufl_extract_constants(item)


def _mk_dolfin_element(ufl_element):
    compiled_element, _ = dolfinx.jit.ffcx_jit(ufl_element)
    ufc_element = compiled_element.create()
    ffi = cffi.FFI()
    return dolfinx.cpp.fem.FiniteElement(ffi.cast("uintptr_t", ufc_element))


def _create_expression_data(quadrature_element: ufl.FiniteElementBase,
                            expr: ufl.core.expr.Expr) -> ExpressionData:
    """Create ExpressionData from a ufl expression. This is mainly for debugging purposes since it provides an easy way
    to obtain some ExpressionData that can be compared against pure ufl forms."""
    if quadrature_element.value_shape() != expr.ufl_shape:
        raise ValueError("Different value shapes for quadrature element and expr.")
    q_points = _determine_quadrature_points_from_element(quadrature_element)
    compiled_expr = dolfinx.jit.ffcx_jit((expr, q_points))
    dolfin_element = _mk_dolfin_element(quadrature_element)
    coefficients = _extract_coefficients(expr)
    constants = _extract_constants(expr)
    ffi = cffi.FFI()
    fptr = ffi.cast("uintptr_t", compiled_expr.tabulate_expression)
    return ExpressionData(fptr, dolfin_element,
                          [coefficients[
                               compiled_expr.original_coefficient_positions[ii]]._cpp_object
                           for ii in range(compiled_expr.num_coefficients)],
                          [constant._cpp_object for constant in constants])


function_data_types = typing.Union[LocalFunction, typing.Tuple[int, int], typing.Any]


def create_expression_data(func_data: function_data_types, finite_element: ufl.FiniteElementBase,
                           coefficients: typing.Iterable[typing.Union[ufl.Coefficient, ufl.core.expr.Expr]] = None,
                           constants: typing.Iterable[ufl.Constant] = None) -> ExpressionData:
    """Wrapper around some constructors and factories for Expression data from dolfinxcoefficients.cpp

    Parameters
    ----------
    func_data
        The data used to construct the actual expression. If integers are considered as pointers. If a single int is
        given, this will be interpreted as a pointer to UFC tabulate_expression function. Two ints are pointers to
        UFC tabulate_tensor functions, where the first is a bilinear form, the second a linear form corresponding to,
        eg, a projection. Another possibility is a dolfinxcoefficients.cpp.LocalFunction, ie. is a point-wise function.

    finite_element
        A ufl finite element of "Quadrature" space

    coefficients
        UFL coefficients on which the expression depends. Can be an ExternalCoefficient provides by this package.

    constants
        UFL constants on which the expression depends.
    """

    if isinstance(func_data, ufl.core.expr.Expr):
        return _create_expression_data(finite_element, func_data)

    def _mk_cpp_coeff(coefficient):
        if isinstance(coefficient, ufl.core.expr.Expr):
            expr_element = ufl.TensorElement("Quadrature", finite_element.cell(), finite_element.degree(),
                                         shape=coefficient.ufl_shape, quad_scheme=finite_element.quadrature_scheme())
            return _create_expression_data(expr_element, coefficient)
        else:
            return coefficient._cpp_object

    coefficients = [] if coefficients is None else coefficients
    constants = [] if constants is None else constants

    if isinstance(func_data, typing.Tuple):
        return _bindings.create_expression_data(*func_data, _mk_dolfin_element(finite_element),
                                                [_mk_cpp_coeff(coefficient) for coefficient in coefficients],
                                                [constant._cpp_object for constant in constants])
    elif isinstance(func_data, LocalFunction):
        return _bindings.create_expression_data(func_data, _mk_dolfin_element(finite_element),
                                                [coefficient._cpp_object for coefficient in coefficients],
                                                [constant._cpp_object for constant in constants])
    else:
        return ExpressionData(func_data, _mk_dolfin_element(finite_element),
                              [coefficient._cpp_object for coefficient in coefficients],
                              [constant._cpp_object for constant in constants])


class ExternalCoefficient(ufl.Coefficient):
    """A coefficient on a quadrature space with expression data attached. In the future, this probably should be a
    subclass of ufl.ExternalCoefficient"""

    def __init__(self, function_space: ufl.FunctionSpace,
                 expression_data: typing.Union[ExpressionData, function_data_types], **kwargs):
        """Constructor

        Parameters
        ----------
        function_space
            The UFL function space for this coefficient. Currently, this must be a Quadrature space
        expression_data
            Either dolfinxcoefficients.ExpressionData or the func_data argument
        **kwargs
            When expression_data is not of type dolfinxcoefficients.ExpressionData, then these are passed to
            create_exressions_data together with expression_data and the finite element of function_space.
        """
        super().__init__(function_space)

        if isinstance(expression_data, ExpressionData):
            if _mk_dolfin_element(function_space.ufl_element()).hash != expression_data.element.hash():
                raise ValueError("Different elements for function space and expression data.")
        else:
            expression_data = create_expression_data(expression_data, function_space.ufl_element(), **kwargs)
        self._cpp_object = expression_data


class Form:
    def __init__(self, form: ufl.Form, form_compiler_parameters: dict = None):
        """Create a class like dolfinx.fem.Form that supports ExternalCoefficients provided by this package.

        Parameters
        ----------
        form
            Pure UFL form
        form_compiler_parameters
            Parameters used in JIT FFCX compilation of this form

        Note
        ----
        This wrapper for UFL form is responsible for the actual FFCX compilation
        and attaching coefficients and domains specific data to the underlying
        C++ Form.
        """
        self.form_compiler_parameters = {} if form_compiler_parameters is None else form_compiler_parameters

        # Extract subdomain data from UFL form
        sd = form.subdomain_data()
        self._subdomains, = list(sd.values())  # Assuming single domain
        domain, = list(sd.keys())  # Assuming single domain
        mesh = domain.ufl_cargo()

        ufc_form = dolfinx.jit.ffcx_jit(
            form,
            form_compiler_parameters=self.form_compiler_parameters,
            mpi_comm=mesh.mpi_comm())

        # For every argument in form extract its function space
        function_spaces = [
            func.ufl_function_space()._cpp_object for func in form.arguments()
        ]

        # Prepare C++ coefficients and constants
        form_coeffs = [form.coefficients()[ufc_form.original_coefficient_position(ii)]
                       for ii in range(ufc_form.num_coefficients)]
        cpp_coeffs = [coeff._cpp_object for coeff in form_coeffs]
        cpp_constants = [constant._cpp_object for constant in form.constants()]

        # Prepare integral data
        ffi = cffi.FFI()
        integral_data = []
        integral_type_names = ["cell", "exterior_facet", "interior_facet", "vertex", "custom"]
        for name in integral_type_names:
            ids = np.zeros(getattr(ufc_form, "num_{:s}_integrals".format(name)), dtype=np.int32)
            if ids.size == 0:
                continue
            ufc_form.get_cell_integral_ids(ffi.cast("int *", ids.ctypes.data))
            integrals = [getattr(ufc_form, "create_{:s}_integral".format(name))(cid) for cid in ids]
            integral_type = getattr(dolfinx.cpp.fem.IntegralType, name)
            integral_data += [
                IntegralData(
                    cid, integral_type, ffi.cast("uintptr_t", integral.tabulate_tensor),
                    [cpp_coeff for ii, cpp_coeff in enumerate(cpp_coeffs) if integral.enabled_coefficients[ii]],
                    cpp_constants, integral.needs_permutation_data)
                for cid, integral in zip(ids, integrals)
            ]

        # Prepare dolfinx.Form and hold it as a member
        self._cpp_object = _bindings.create_form(integral_data, function_spaces)

        if mesh is None:
            raise RuntimeError("Expecting to find a Mesh in the form.")

        # Attach mesh (because function spaces and coefficients may be
        # empty lists)
        if not function_spaces:
            self._cpp_object.set_mesh(mesh)

        # Attach subdomains to C++ Form if we have them
        subdomains = self._subdomains.get("cell")
        if subdomains:
            self._cpp_object.set_cell_domains(subdomains)

        subdomains = self._subdomains.get("exterior_facet")
        if subdomains:
            self._cpp_object.set_exterior_facet_domains(subdomains)

        subdomains = self._subdomains.get("interior_facet")
        if subdomains:
            self._cpp_object.set_interior_facet_domains(subdomains)

        subdomains = self._subdomains.get("vertex")
        if subdomains:
            self._cpp_object.set_vertex_domains(subdomains)
