#!/usr/bin/env python
# coding: utf-8

# based on the Cahn-Hilliard demo of [dolfinx](https://github.com/FEniCS/dolfinx)

# In[ ]:


import os

import numpy as np
from mpi4py import MPI
from petsc4py import PETSc

from dolfinx import (Form, Function, FunctionSpace, NewtonSolver,
                     NonlinearProblem, UnitSquareMesh, log)
from dolfinx.cpp.mesh import CellType
from dolfinx.fem.assemble import assemble_matrix, assemble_vector
from dolfinx.io import XDMFFile
from ufl import (FiniteElement, VectorElement, TestFunctions, TrialFunction, derivative, diff,
                 dx, grad, inner, split, variable)


# In[ ]:


import ufl
import ufl.core.expr
import dolfinx.jit
import dolfinx.cpp.fem
import dolfinxcoefficients
import cffi


# In[ ]:


ffi = cffi.FFI()

# In[ ]:


# Create mesh and build function space
mesh = UnitSquareMesh(MPI.COMM_WORLD, 1, 1, CellType.triangle)
P1 = FiniteElement("Lagrange", mesh.ufl_cell(), 1)
ME = FunctionSpace(mesh, P1 * P1)


# In[ ]:


# Model parameters
lmbda = dolfinx.Constant(mesh, 1.0e-02)# surface parameter
dt = dolfinx.Constant(mesh, 5.0e-06)   # time step
theta = dolfinx.Constant(mesh, 0.5)    # time stepping family, e.g. theta=1 -> backward Euler, theta=0.5 -> Crank-Nicolson


# In[ ]:


# Define trial and test functions
du = TrialFunction(ME)
q, v = TestFunctions(ME)


# In[ ]:


# Define functions
u = Function(ME)  # current solution
u0 = Function(ME)  # solution from previous converged step


# In[ ]:


# Split mixed functions
dc, dmu = split(du)
c, mu = split(u)
c0, mu0 = split(u0)


# In[ ]:


def u_init(x):
    """Initialise values for c and mu."""
    values = np.zeros((2, x.shape[1]))
    #values[0] = 0.63 + 0.02 * (0.5 - np.random.rand(x.shape[1]))
    values[0] = x[0,:]
    return values

# Create initial conditions and interpolate
u.interpolate(u_init)


# Compute the chemical potential df/dc
c = variable(c)
f = 100 * c**2 * (1 - c)**2
dfdc = diff(f, c)

# mu_(n+theta)
mu_mid = (1.0 - theta) * mu0 + theta * mu


# ExternalCoefficients
def FS(elmt):
    return ufl.FunctionSpace(mesh.ufl_domain(), elmt)

gradc_space = FS(VectorElement("Quadrature", mesh.ufl_cell(), degree=0, quad_scheme="default"))
gradc = dolfinxcoefficients.ExternalCoefficient(gradc_space, grad(c))

# a first test
_dx = dx(
    domain=mesh, metadata={"quadrature_degree": 0, "quadrature_rule": "default"})
da = dolfinx.fem.assemble_scalar(ufl.inner(grad(c), grad(c)) * _dx)
ma = dolfinx.fem.assemble_scalar(dolfinxcoefficients.Form(ufl.inner(gradc, gradc) * _dx)._cpp_object)

assert ma == da



dfdc_space = FS(FiniteElement("Quadrature", mesh.ufl_cell(), degree=2, quad_scheme="default"))
dfdc = dolfinxcoefficients.ExternalCoefficient(dfdc_space, diff(f, c))

dfdcdu_space = FS(VectorElement("Quadrature", mesh.ufl_cell(), degree=2, quad_scheme="default"))
dfdcdu = dolfinxcoefficients.ExternalCoefficient(dfdcdu_space, diff(diff(f, c), u))

mu_mid_space = FS(FiniteElement("Quadrature", mesh.ufl_cell(), degree=2, quad_scheme="default"))
mu_mid = dolfinxcoefficients.ExternalCoefficient(mu_mid_space, (1.0 - theta) * mu0 + theta * mu)


gradmu_mid_space = FS(VectorElement("Quadrature", mesh.ufl_cell(), degree=0, quad_scheme="default"))
gradmu_mid = dolfinxcoefficients.ExternalCoefficient(gradmu_mid_space, grad((1.0 - theta) * mu0 + theta * mu))


# Weak statement of the equations
L0 = inner(c, q) * dx - inner(c0, q) * dx + dt * inner(grad((1.0 - theta) * mu0 + theta * mu), grad(q)) * dx(
    domain=mesh, metadata={"quadrature_degree": 0, "quadrature_rule": "default"})
L1 = inner(mu, v) * dx - inner(diff(f, c), v) * dx(
    domain=mesh, metadata={"quadrature_degree": 2, "quadrature_rule": "default"}) \
     - lmbda * inner(grad(c), grad(v)) * dx(
    domain=mesh, metadata={"quadrature_degree": 0, "quadrature_rule": "default"})
L_orig = L0 + L1


# Weak statement of the equations
L0 = inner(c, q) * dx - inner(c0, q) * dx + dt * inner(gradmu_mid, grad(q)) * dx(
    domain=mesh, metadata={"quadrature_degree": 0, "quadrature_rule": "default"})
L1 = inner(mu, v) * dx - inner(dfdc, v) * dx(
    domain=mesh, metadata={"quadrature_degree": 2, "quadrature_rule": "default"}) \
     - lmbda * inner(gradc, grad(v)) * dx(
    domain=mesh, metadata={"quadrature_degree": 0, "quadrature_rule": "default"})
L = L0 + L1

#
# L_orig = inner(diff(f, c), v) * dx(
#     domain=mesh, metadata={"quadrature_degree": 2, "quadrature_rule": "default"})
# L = inner(dfdc, v) * dx(
#     domain=mesh, metadata={"quadrature_degree": 2, "quadrature_rule": "default"})
#
# L_orig = lmbda * inner(grad(c), grad(v)) * dx(
#     domain=mesh, metadata={"quadrature_degree": 0, "quadrature_rule": "default"})
# L = lmbda * inner(gradc, grad(v)) * dx(
#     domain=mesh, metadata={"quadrature_degree": 0, "quadrature_rule": "default"})
#
# L_orig = dt * inner(grad((1.0 - theta) * mu0 + theta * mu), grad(q)) * dx(
#     domain=mesh, metadata={"quadrature_degree": 0, "quadrature_rule": "default"})
#
# L = dt * inner(gradmu_mid, grad(q)) * dx(
#     domain=mesh, metadata={"quadrature_degree": 0, "quadrature_rule": "default"})

dL = dolfinx.Form(L_orig)
mL = dolfinxcoefficients.Form(L)

dF = assemble_vector(dL)
dF.ghostUpdate(addv=PETSc.InsertMode.ADD, mode=PETSc.ScatterMode.REVERSE)
print(dF.array)

mF = assemble_vector(mL._cpp_object)
mF.ghostUpdate(addv=PETSc.InsertMode.ADD, mode=PETSc.ScatterMode.REVERSE)
print(mF.array)

assert np.allclose(dF.array, mF.array, atol=1e-8, rtol=1e-8)

#
# # In[ ]:
#
#
# # Compute directional derivative about u in the direction of du (Jacobian)
# a = derivative(L, u, du, coefficient_derivatives={
#     dfdc: ufl.inner(dfdcdu, du),
#     gradc: grad(dc),
#     gradmu_mid: grad(theta * dmu)
# })
#
#
# # In[ ]:
#
#
# print(derivative(inner(mu,v), u, du).ufl_shape)
# print(derivative(lmbda * inner(grad(dc), grad(v)), u, du, coefficient_derivatives={gradc: grad(dc)}).ufl_shape)
# print(derivative(inner(dfdc, v), u, du, coefficient_derivatives={dfdc: ufl.inner(dfdcdu, du)}).ufl_shape)
#
#
# # In[ ]:
#
#
# dolfinx.Form(lmbda * inner(grad(dc), grad(v))*dx(
#     domain=mesh, metadata={"quadrature_degree": 0, "quadrature_rule": "default"}))
#
#
# # In[ ]:
#
#
# get_ipython().run_line_magic('pinfo2', 'ufl.derivative')
#
#
# # In[ ]:
#
# # NOTE: UFL coefficient derivatives can't handle this case properly!
# dolfinx.Form(derivative(lmbda * inner(gradc, grad(v)), u, du, coefficient_derivatives={gradc: grad(dc)})*
#              dx(
#                  domain=mesh, metadata={"quadrature_degree": 0, "quadrature_rule": "default"}))
#
#
# # In[ ]:
#
# dolfinx.Form(L)
# cL = dolfinxcoefficients.Form(L)
#
#
# # In[ ]:
#
#
#
#
#
# # In[ ]:
#
#
# # TODO: wrap the integrals!
#
#
# # In[ ]:
#
#
# cform = dolfinx.jit.ffcx_jit(L)
#
#
# # In[ ]:
#
#
# cform.num_vertex_integrals
#
#
# # In[ ]:
#
#
# dolfinx.cpp.fem.FormIntegrals.Type.exterior_facet
#
#
# # In[ ]:
#
#
# ids = np.zeros(1, dtype=np.int32)
# cform.get_cell_integral_ids(ffi.cast("int *", ids.ctypes.data))
#
#
# # In[ ]:
#
#
# integrals = [cform.create_cell_integral(cid) for cid in ids]
#
#
# # In[ ]:
#
#
# integrals[0].enabled_coefficients
#
#
# # In[ ]:
#
#
# form_coeffs = [L.coefficients()[cform.original_coefficient_position(ii)]
#                for ii in range(cform.num_coefficients)]
#
#
# # In[ ]:
#
#
# cpp_coeffs = [expression_data[coeff].element if coeff in expression_data.keys() else coeff._cpp_object
#               for coeff in form_coeffs]
# cpp_constants = [constant._cpp_object for constant in L.constants()]
# cpp_function_spaces = [argument.ufl_function_space()._cpp_object for argument in L.arguments()]
#
#
# # In[ ]:
#
#
# integral_data = [
#     dolfinxcoefficients.cpp.IntegralData(cid,
#                                       dolfinx.cpp.fem.FormIntegrals.Type.cell,
#                                       ffi.cast("uintptr_t", integral.tabulate_tensor),
#                                       cpp_coeffs, cpp_constants)
#     for cid, integral in zip(ids, integrals)
# ]
#
#
# # In[ ]:
#
#
# form_L = dolfinxcoefficients.cpp.create_form(
#     integral_data,
#     {vv.element: vv for vv in expression_data.values()},
#     cpp_function_spaces
# )
#
#
# # In[ ]:
#
#
# gradc_data.element.signature()
#
#
# # In[ ]:
#
#
# gradc_elmt.value_shape()
#
#
# # In[ ]:
#
#
# gradc.ufl_shape
#
#
# # In[ ]:
#
# class CahnHilliardEquation(NonlinearProblem):
#     def __init__(self, a, L):
#         super().__init__()
#         self.L = Form(L)
#         self.a = Form(a)
#         self._F = None
#         self._J = None
#
#     def form(self, x):
#         x.ghostUpdate(addv=PETSc.InsertMode.INSERT, mode=PETSc.ScatterMode.FORWARD)
#
#     def F(self, x):
#         if self._F is None:
#             self._F = assemble_vector(self.L)
#         else:
#             with self._F.localForm() as f_local:
#                 f_local.set(0.0)
#             self._F = assemble_vector(self._F, self.L)
#         self._F.ghostUpdate(addv=PETSc.InsertMode.ADD, mode=PETSc.ScatterMode.REVERSE)
#         return self._F
#
#     def J(self, x):
#         if self._J is None:
#             self._J = assemble_matrix(self.a)
#         else:
#             self._J.zeroEntries()
#             self._J = assemble_matrix(self._J, self.a)
#         self._J.assemble()
#         return self._J
#
# # In[ ]:
#
# # Create nonlinear problem and Newton solver
# problem = CahnHilliardEquation(a, L)
# solver = NewtonSolver(MPI.COMM_WORLD)
# solver.convergence_criterion = "incremental"
# solver.rtol = 1e-6
#
#
# # In[ ]:
#
#
# # Output file
# file = XDMFFile(MPI.COMM_WORLD, "output.xdmf", "w")
# file.write_mesh(mesh)
#
# # Step in time
# t = 0.0
#
# # Check if we are running on CI server and reduce run time
# if "CI" in os.environ.keys() or "GITHUB_ACTIONS" in os.environ.keys():
#     T = 3 * dt
# else:
#     T = 50 * dt
#
# u.vector.copy(result=u0.vector)
# u0.vector.ghostUpdate(addv=PETSc.InsertMode.INSERT, mode=PETSc.ScatterMode.FORWARD)
#
# while (t < T):
#     t += dt
#     r = solver.solve(problem, u.vector)
#     print("Step, num iterations:", int(t / dt), r[0])
#     u.vector.copy(result=u0.vector)
#     file.write_function(u.sub(0), t)
#
# file.close()
#

# In[ ]:



