# Python interface of dolfinxcoefficients, a framework agnostic dolfinx extension for externally evaluated form coefficients 

This package requires the installation of the C++ package `dolfinxcoefficients`. In addition to the necessary bindings it
provides as small set of helpers and tools that easy the use of the C++ core package. 


## Installation

Standard Python setuptools installation. Requires `dolfinx` and `pybind11` version >= 2.5.0.


## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.

