// Copyright (C) 2019-2020 Matthias Rambausek
//
// This file is part of 'materiaux' (https://gitlab.com/materiaux/dolfinxcoeffcients)
//
// SPDX-License-Identifier:    AGPL-3.0-or-later

#pragma once

#include <dolfinx/fem/Form.h>
#include <dolfinx/fem/FormCoefficients.h>
#include <dolfinx/fem/FormIntegrals.h>
#include <dolfinx/function/Constant.h>
#include <dolfinx/function/Function.h>
#include <dolfinx/function/FunctionSpace.h>
#include <dolfinx/fem/FiniteElement.h>
#include <functional>
#include <variant>

namespace dolfinxcoefficients {

    using scalar_t = PetscScalar;

    using tabulate_tensor_t = void (*)(scalar_t *A, const scalar_t *w, const scalar_t *c,
                                       const double *coordinate_dofs, const int *local_index,
                                       const uint8_t *quadrature_permutation,
                                       const uint32_t cell_permutation);
    using tabulate_tensor_func = decltype(std::function{std::declval<tabulate_tensor_t>()});

    using tabulate_expression_t = void (*)(scalar_t *A, const scalar_t *w, const scalar_t *c,
                                           const double *coordinate_dofs);
    using tabulate_expression_func = decltype(std::function{std::declval<tabulate_expression_t>()});

    using local_function_t = void (*)(scalar_t *, const scalar_t *, const scalar_t *);
    using local_function = decltype(std::function{std::declval<local_function_t>()});

    /// As small wrapper around the std::function. This avoids extra materiaux layers around
    /// std::function.
    struct LocalFunction {
        const local_function function;
    };

    // Note: Function is not const because it needs to be passed into FormCoefficients as non-const
    using Function = std::shared_ptr<dolfinx::function::Function<scalar_t>>;
    using Constant = std::shared_ptr<const dolfinx::function::Constant<scalar_t>>;
    struct ExpressionData;
    using ExternalCoefficient = std::shared_ptr<const ExpressionData>;
    using FiniteElement = std::shared_ptr<const dolfinx::fem::FiniteElement>;

    /// Collecting types that represent a field either directly through identity or by computation.
    using Coefficient = std::variant<Function, ExternalCoefficient>;

    /// Data to setup the computation of a dependent coefficient
    struct ExpressionData {
        /// Create an expression from a ffcx-type expression. All field data provided must refer to
        /// the "total" data, i.e. the data for all evaluation/quadrature points.
        ExpressionData(tabulate_expression_func expression, FiniteElement finite_element,
                       std::vector<Coefficient> coefficients, std::vector<Constant> constants);
        const tabulate_expression_func tabulate_expression;
        const FiniteElement element;
        const std::vector<Coefficient> coefficients;
        const std::vector<Constant> constants;
    };

    /// Holding data for a form
    struct IntegralData {
        /// Create IntegralData from a FormIntegrals entry and coefficient/constant information.
        /// @param id: the id/number of the integral (same as in dolfinx::FormIntegrals)
        /// @param type: the domain type
        /// @param tabulate_tensor: the actual function
        /// @param coefficients: coefficient information for this integral
        /// @param constants: constants for this integral
        IntegralData(int id, dolfinx::fem::IntegralType type,
                     tabulate_tensor_func tabulate_tensor, std::vector<Coefficient> coefficients,
                     std::vector<Constant> constants, bool needs_permutation_data);
        const int id;
        const dolfinx::fem::IntegralType type;
        const tabulate_tensor_func tabulate_tensor;
        const std::vector<Coefficient> coefficients;
        const std::vector<Constant> constants;
        const bool needs_permutation_data;
    };

    // NOTE: this can be simplified if the integrals all come from the same form and the
    //  external coefficients can be packated in the same way as dolfin::Functions.

    /// Create a Form object (finds all terminal coefficients and constants)
    /// @param integrals: Currently following the pattern of dolfinx::FormIntegrals entries
    /// @param dependent_coefficients: additional information about coefficients that need to be
    /// computed (via the provided ExpressionData)
    /// @param function_spaces: FunctionSpaces (Argument spaces) of the form
    dolfinx::fem::Form<scalar_t>
    create_form(const std::vector<std::shared_ptr<IntegralData>> &integrals,
                const std::vector<std::shared_ptr<const dolfinx::function::FunctionSpace>>
                    &function_spaces);

    /// This type prepares the individual coefficients and constants of the original form based on
    /// the coefficients and constants from the wrapping form. The element data represents the
    /// resulting field for the original form or whoever consumes this preprocessor.
    using FieldPreprocessor = tabulate_expression_func;

    /// Create an expression from a projection. All field data provided must refer to the "total"
    /// data, i.e. the data for all evaluation/quadrature points.
    /// Note that currently this only works for cells. A generalization is only possible is the
    /// results is a CallableData<tabulate_tensor_func>.
    ExpressionData create_expression_data(tabulate_tensor_func form_a, tabulate_tensor_func form_L,
                                          FiniteElement target_element,
                                          std::vector<Coefficient> coefficients,
                                          std::vector<Constant> constants);

    /// Create an expression from a "local" function. All field data provided must refer to the
    /// "total" data, i.e. the data for all evaluation/quadrature points.
    ExpressionData create_expression_data(local_function function, FiniteElement finite_element,
                                          std::vector<Coefficient> coefficients,
                                          std::vector<Constant> constants);

    /// Create an expression from a "local" function wrapper.
    inline ExpressionData create_expression_data(LocalFunction function,
                                                 FiniteElement target_element,
                                                 std::vector<Coefficient> coefficients,
                                                 std::vector<Constant> constants) {
        return create_expression_data(function.function, std::move(target_element),
                                      std::move(coefficients), std::move(constants));
    }

} // namespace dolfinxcoefficients
