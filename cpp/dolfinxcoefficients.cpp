// Copyright (C) 2019-2020 Matthias Rambausek
//
// This file is part of 'materiaux' (https://gitlab.com/materiaux/dolfinxcoeffcients)
//
// SPDX-License-Identifier:    AGPL-3.0-or-later

#include "dolfinxcoefficients.h"
#include <Eigen/Dense>
#include <algorithm>

namespace dolfinxcoefficients {

    namespace {
        /// Check whether a pointer is null
        template<typename T, std::enable_if_t<std::is_pointer_v<T>, int> = 0>
        bool is_null(const T &p) {
            return p == nullptr;
        }

        /// Check whether a shared_ptr pointer is null
        template<typename T>
        bool is_null(const std::shared_ptr<T> &p) { return p == nullptr; }

        /// Check whether Coefficient (Function of FiniteElement) holds a nullptr
        inline bool is_null(const Coefficient &coefficient) {
            auto check = [](auto &&item) { return item == nullptr; };
            return std::visit([&](auto &&item) -> bool { return check(item); }, coefficient);
        };

        /// Check whether any item in the given range is null
        template<typename InputIt>
        bool any_null(InputIt first, InputIt last) {
            if (first == last)
                return false;
            else
                return std::any_of(first, last, [](const auto &item) { return is_null(item); });
        }

        /// Size of the expansion coefficients (per element) of a Function
        int size(const Function &func) {
            return func->function_space()->element()->space_dimension();
        };

        /// Size of the expansion coefficients of an ExternalCoefficient
        int size(const ExternalCoefficient &coeff) { return coeff->element->space_dimension(); };

        /// Size of the Constant data array
        int size(const Constant &constant) { return constant->value.size(); };

        /// Size of a Coefficient (Function or Finite Element)
        int size(const Coefficient &coeff) {
            return std::visit([](const auto &item) { return size(item); }, coeff);
        }

        /// Extract the FiniteElement of a Function
        FiniteElement element(const Function &func) { return func->function_space()->element(); };

        /// Extract the FiniteElement of an ExternalCoefficient
        FiniteElement element(const ExternalCoefficient &coeff) { return coeff->element; };

        /// Extract the FiniteElement of a Coefficient
        FiniteElement element(const Coefficient &coeff) {
            return std::visit([](const auto &item) { return element(item); }, coeff);
        }

        /// "independent" trait implementation for Constant
        bool is_independent(const Constant &constant) { return false; }

        /// "independent" trait implementation for Function
        bool is_independent(const Function &coeff) { return true; }

        /// "independent" trait implementation for ExternalCoefficient
        bool is_independent(const ExternalCoefficient &coeff) { return false; }

        /// "independent" trait implementation for Coefficient
        bool is_independent(const Coefficient &coeff) {
            return std::visit([](auto &&item) -> bool { return is_independent(item); }, coeff);
        }

        /// Computes the total array size covered by the range of items (first : last). Items must
        /// not be null. Requires an appropriate overload of "size" for item.
        template<typename InputIt>
        int fields_size(InputIt first, InputIt last) {
            assert(!any_null(first, last));
            return std::accumulate(first, last, int{0}, [](int sum, const decltype(*first) &field) {
                return sum + size(field);
            });
        }

        /// Creates a vector with the offsets of the items in (first : last). Items must not be
        /// null. Requires an appropriate overload of "size" for item.
        template<typename InputIt>
        std::vector<int> field_offsets(InputIt first, InputIt last) {
            assert(!any_null(first, last));
            return std::accumulate(first, last, std::vector<int>{{0}},
                                   [](std::vector<int> offsets, const decltype(*first) &field) {
                                       offsets.push_back(offsets.back() + size(field));
                                       return std::move(offsets);
                                   });
        }

        /// Covenience function for checking for nullptrs.
        template<typename InputIt>
        void check_coefficients(InputIt first, InputIt last) {
            if (any_null(first, last))
                throw std::invalid_argument("There was a nullptr detected in coefficients.");
        }

        /// Covenience function for checking for nullptrs.
        template<typename Container>
        void check_coefficients(Container &&container) {
            if (!container.empty())
                check_coefficients(begin(container), end(container));
        }

        /// Covenience function for checking for nullptrs.
        template<typename InputIt>
        void check_constants(InputIt first, InputIt last) {
            if (any_null(first, last))
                throw std::invalid_argument("There was a nullptr detected in constants.");
        }

        /// Covenience function for checking for nullptrs.
        template<typename Container>
        void check_constants(Container &&container) {
            if (!container.empty())
                check_constants(begin(container), end(container));
        }

        /// Apply preprocessors on the given offsets and with the given inputs.
        /// @param[in] first preprocessor (iterator over preprocessor)
        /// @param[in] last preprocessor (iterator over preprocessor)
        /// @param[in] offset iterator over offsets wrt. to element
        /// @param[in,out] target field the preprocessor are applied to
        /// @param[in] w all coefficients
        /// @param[in] c all constants
        /// @param[in] coordinate_dofs coordinate dofs
        template<typename PPIt, typename OffsetIt>
        void apply_preprocessors(PPIt first, PPIt last, OffsetIt offset, scalar_t *target,
                                 const scalar_t *w, const scalar_t *c,
                                 const double *coordinate_dofs) {
            std::for_each(first, last, [=](const FieldPreprocessor &preprocessor) mutable -> void {
                preprocessor(target + *offset, w, c, coordinate_dofs);
                std::advance(offset, 1);
            });
        }

        using PPKey = std::variant<Function, Constant, ExternalCoefficient>;
        struct PPData {
            FieldPreprocessor pp_func;
            std::vector<Coefficient> coefficients;
            std::vector<Constant> constants;
        };
        using pp_data_container = std::map<PPKey, PPData>;
        using pp_container = std::map<PPKey, FieldPreprocessor>;

        /// Create the preprocessor for given "pp_item" based on "data" and add it to
        /// element. It "pp_item" already has an entry in element, nothing is to be done.
        /// @param target: storage for created preprocessors
        /// @param pp_item: data key
        /// @param data: container for data from which the preprocessors are finally created
        pp_container create_preprocessor(pp_container target, const PPKey &pp_item,
                                         const pp_data_container &data) {
            // Check if preprocessor already extists
            if (target.find(pp_item) != end(target))
                return std::move(target);

            const auto &pp_data = data.at(pp_item);

            // Check if preprocessor is trivial
            if (size(pp_data.coefficients) == 0 && size(pp_data.constants) == 0) {
                target[pp_item] = pp_data.pp_func;
                return std::move(target);
            }

            // Determine offsets of the individual preprocessors for the expression argument
            // fields
            auto pp_coefficient_offsets =
                    field_offsets(begin(pp_data.coefficients), end(pp_data.coefficients));
            auto pp_constant_offsets =
                    field_offsets(begin(pp_data.constants), end(pp_data.constants));

            // Create preprocessors for coefficients
            const auto coefficient_preprocessors = std::accumulate(
                    begin(pp_data.coefficients), end(pp_data.coefficients),
                    std::vector<FieldPreprocessor>{},
                    [&](std::vector<FieldPreprocessor> coeff_pps, const Coefficient &coeff) {
                        // convert coefficient to PPKey
                        PPKey key{std::visit([](const auto &item) { return PPKey{item}; }, coeff)};
                        // recurse to resolve dependencies
                        target = create_preprocessor(std::move(target), key, data);
                        coeff_pps.emplace_back(target.at(key));
                        return std::move(coeff_pps);
                    });

            // Create preprocessors for constants
            const auto constant_preprocessors = std::accumulate(
                    begin(pp_data.constants), end(pp_data.constants), std::vector<FieldPreprocessor>{},
                    [&](std::vector<FieldPreprocessor> constant_pps, const Constant &constant) {
                        PPKey key{constant};
                        // recurse to resolve dependencies
                        target = create_preprocessor(std::move(target), key, data);
                        constant_pps.emplace_back(target.at(key));
                        return std::move(constant_pps);
                    });

            // Prepare scratch arrays
            Eigen::Array<scalar_t, Eigen::Dynamic, 1> _coefficients(pp_coefficient_offsets.back());
            Eigen::Array<scalar_t, Eigen::Dynamic, 1> _constants(pp_constant_offsets.back());

            target[pp_item] = {[=, tabulate = pp_data.pp_func](
                    scalar_t *w_target, const scalar_t *w, const scalar_t *c,
                    const double *coordinate_dofs) mutable -> void {
                assert(size(pp_coefficient_offsets) - 1 == size(coefficient_preprocessors));
                assert(size(pp_constant_offsets) - 1 == size(constant_preprocessors));
                // Preprocess global coefficients and constants
                _coefficients.setZero();
                apply_preprocessors(cbegin(coefficient_preprocessors),
                                    cend(coefficient_preprocessors), cbegin(pp_coefficient_offsets),
                                    _coefficients.data(), w, c, coordinate_dofs);
                _constants.setZero();
                apply_preprocessors(cbegin(constant_preprocessors), cend(constant_preprocessors),
                                    cbegin(pp_constant_offsets), _constants.data(), w, c,
                                    coordinate_dofs);
                // Evaluate
                tabulate(w_target, _coefficients.data(), _constants.data(), coordinate_dofs);
            }};
            return std::move(target);
        }

        tabulate_tensor_func wrap_integral(const IntegralData &integral_data,
                                           const pp_container &preprocessors) {
            // Determine offsets of the individual preprocessors
            auto pp_coefficient_offsets =
                    field_offsets(begin(integral_data.coefficients), end(integral_data.coefficients));
            auto pp_constant_offsets =
                    field_offsets(begin(integral_data.constants), end(integral_data.constants));

            // Collect coefficient preprocessors
            const auto coefficient_preprocessors = std::accumulate(
                    begin(integral_data.coefficients), end(integral_data.coefficients),
                    std::vector<FieldPreprocessor>{},
                    [&](std::vector<FieldPreprocessor> coeff_pps, const Coefficient &coeff) {
                        // convert coefficient to PPKey
                        PPKey key{std::visit([](const auto &item) { return PPKey{item}; }, coeff)};
                        coeff_pps.emplace_back(preprocessors.at(key));
                        return std::move(coeff_pps);
                    });

            // Collect constant preprocessors
            const auto constant_preprocessors = std::accumulate(
                    begin(integral_data.constants), end(integral_data.constants),
                    std::vector<FieldPreprocessor>{},
                    [&](std::vector<FieldPreprocessor> constant_pps, const Constant &constant) {
                        PPKey key{constant};
                        constant_pps.emplace_back(preprocessors.at(key));
                        return std::move(constant_pps);
                    });

            // Prepare scratch array
            Eigen::Array<scalar_t, Eigen::Dynamic, 1> _coefficients(pp_coefficient_offsets.back());
            Eigen::Array<scalar_t, Eigen::Dynamic, 1> _constants(pp_constant_offsets.back());

            return [=, tabulate = integral_data.tabulate_tensor](
                    scalar_t *w_target, const scalar_t *w, const scalar_t *c,
                    const double *coordinate_dofs, const int *local_index,
                    const uint8_t *quadrature_permutation,
                    const uint32_t cell_permutation) mutable -> void {
                assert(size(pp_coefficient_offsets) - 1 == size(coefficient_preprocessors));
                assert(size(pp_constant_offsets) - 1 == size(constant_preprocessors));
                // Preprocess global coefficients and constants
                _coefficients.setZero();
                apply_preprocessors(cbegin(coefficient_preprocessors),
                                    cend(coefficient_preprocessors), cbegin(pp_coefficient_offsets),
                                    _coefficients.data(), w, c, coordinate_dofs);
                _constants.setZero();
                apply_preprocessors(cbegin(constant_preprocessors), cend(constant_preprocessors),
                                    cbegin(pp_constant_offsets), _constants.data(), w, c,
                                    coordinate_dofs);
                // Evaluate
                tabulate(w_target, _coefficients.data(), _constants.data(), coordinate_dofs,
                         local_index, quadrature_permutation, cell_permutation);
            };
        }

        /// Helper to recursively collect ExternalCoefficients
        std::vector<ExternalCoefficient>
        append_external_coefficients(std::vector<ExternalCoefficient> external_coefficients,
                                     const std::vector<Coefficient> &coefficients) {
            return std::accumulate(
                    begin(coefficients), end(coefficients), std::move(external_coefficients),
                    [](std::vector<ExternalCoefficient> external_coefficients,
                       const Coefficient &coefficient) {
                        // if coefficient is "external"
                        if (const auto external_coefficient_ptr =
                                    std::get_if<ExternalCoefficient>(&coefficient);
                                external_coefficient_ptr) {
                            const auto &external_coefficient = *external_coefficient_ptr;
                            external_coefficients.push_back(external_coefficient);
                            // recurse since it has further possibly external coefficients
                            return append_external_coefficients(std::move(external_coefficients),
                                                                external_coefficient->coefficients);
                        } else {
                            return std::move(external_coefficients);
                        }
                    });
        }

        /// Helper to extract Functions from an object with Coefficients
        template<typename T>
        std::vector<Function> append_functions(std::vector<Function> target, const T &data) {
            return std::accumulate(begin(data->coefficients), end(data->coefficients), target,
                                   [](std::vector<Function> target, const Coefficient &coeff) {
                                       // if coefficient is a Function
                                       if (auto func = std::get_if<Function>(&coeff); func)
                                           target.push_back(*func);
                                       return std::move(target);
                                   });
        }

        /// Helper to extract Constants from an object with a Constants
        template<typename T>
        std::vector<Constant> append_constants(std::vector<Constant> target, const T &data) {
            return std::accumulate(begin(data->constants), end(data->constants), target,
                                   [](std::vector<Constant> target, const Constant &constant) {
                                       target.push_back(constant);
                                       return std::move(target);
                                   });
        }

        /// Gather data that can is required to create actual coefficient preprocessors
        pp_data_container
        prepare_pp_data(const std::vector<Function> &functions,
                        const std::vector<Constant> &constants,
                        const std::vector<ExternalCoefficient> &external_coefficients) {

            // Determine offsets
            auto coefficient_offsets = field_offsets(begin(functions), end(functions));
            auto constant_offsets = field_offsets(begin(constants), end(constants));

            // Create copy data for Function expansion coefficients
            auto preprocessor_data = std::accumulate(
                    begin(functions), end(functions), pp_data_container{},
                    [offset_it = begin(coefficient_offsets)](pp_data_container data,
                                                             const Function &func) mutable -> auto {
                        const auto _offset = *offset_it;
                        const auto _size = size(func);
                        data.emplace(func,
                                     PPData{[=](scalar_t *target, const scalar_t *w, const scalar_t *c,
                                                const double *coordinate_dofs) {
                                         std::copy(w + _offset, w + _offset + _size, target);
                                     },
                                            {},
                                            {}});
                        std::advance(offset_it, 1);
                        return std::move(data);
                    });

            // Create copy data for Constants (simple array data)
            preprocessor_data = std::accumulate(
                    begin(constants), end(constants), preprocessor_data,
                    [offset_it = begin(constant_offsets)](pp_data_container data,
                                                          const Constant &constant) mutable -> auto {
                        const auto _offset = *offset_it;
                        const auto _size = size(constant);
                        data.emplace(constant,
                                     PPData{[=](scalar_t *target, const scalar_t *w, const scalar_t *c,
                                                const double *coordinate_dofs) {
                                         std::copy(c + _offset, c + _offset + _size, target);
                                     },
                                            {},
                                            {}});
                        std::advance(offset_it, 1);
                        return std::move(data);
                    });

            // Create preprocessor data for external coefficients, i.e. from ExpressionData
            preprocessor_data = std::accumulate(
                    begin(external_coefficients), end(external_coefficients),
                    std::move(preprocessor_data),
                    [](pp_data_container data, const ExternalCoefficient &external_coefficient) {
                        data.emplace(external_coefficient,
                                     PPData{external_coefficient->tabulate_expression,
                                            external_coefficient->coefficients,
                                            external_coefficient->constants});
                        return std::move(data);
                    });
            return preprocessor_data;
        } // namespace

        /// Convenience function encapsulating the process of preprocessor creation
        pp_container create_preprocessors(const pp_data_container &preprocessor_data) {
            return std::accumulate(begin(preprocessor_data), end(preprocessor_data), pp_container{},
                                   [&](pp_container target, const auto &pair) {
                                       target = create_preprocessor(std::move(target), pair.first,
                                                                    preprocessor_data);
                                       return std::move(target);
                                   });
        }

        /// Convenience function encapsulating the process of FormIntegrals creation
        dolfinx::fem::FormIntegrals<scalar_t>
        create_form_integrals(const std::vector<std::shared_ptr<IntegralData>> &integrals,
                              const pp_container &preprocessors) {
            using typed_integrals = std::vector<std::pair<
                    int, std::function<void(scalar_t *, const scalar_t *, const scalar_t *, const double *,
                                            const int *, const std::uint8_t *,
                                            const std::uint32_t)>>>;
            using integral_map_t = std::map<dolfinx::fem::IntegralType, typed_integrals>;
            integral_map_t integral_map{};
            bool needs_permutation_info = false;
            std::for_each(begin(integrals), end(integrals),
                          [&](const std::shared_ptr<IntegralData> &data) {
                              if (integral_map.count(data->type) == 0)
                                  integral_map[data->type] = typed_integrals{};
                              integral_map[data->type].emplace_back(data->id, wrap_integral(*data, preprocessors));
                              needs_permutation_info = needs_permutation_info || data->needs_permutation_data;
                          });
            return {integral_map, needs_permutation_info};
        }

        /// Helper function encapsulating the process of FormCoefficients creation
        dolfinx::fem::FormCoefficients<scalar_t>
        create_form_coefficients(const std::vector<Function> &functions) {
            using coeff_vector_t = std::vector<std::tuple<int, std::string, Function>>;
            return {std::accumulate(
                    begin(functions), end(functions), coeff_vector_t{},
                    [ii = 0](coeff_vector_t target, const Function &func) mutable -> coeff_vector_t {
                        target.emplace_back(ii++, func->name, func);
                        return std::move(target);
                    })};
        }

        /// Helper function encapsulating the process of FormConstants creation
        std::vector<std::pair<std::string, Constant>>
        create_form_constants(const std::vector<Constant> &constants) {
            using constant_vector_t = std::vector<std::pair<std::string, Constant>>;
            return std::accumulate(begin(constants), end(constants), constant_vector_t{},
                                   [](constant_vector_t target, const Constant &constant) {
                                       target.emplace_back("c_" + std::to_string(size(target)),
                                                           constant);
                                       return std::move(target);
                                   });
        }

        /// Collect all external coefficients from integrals
        std::vector<ExternalCoefficient>
        gather_external_coefficients(const std::vector<std::shared_ptr<IntegralData>> &integrals) {
            auto external_coefficients = std::accumulate(
                    begin(integrals), end(integrals), std::vector<ExternalCoefficient>{},
                    [](std::vector<ExternalCoefficient> external_coefficients,
                       const std::shared_ptr<IntegralData> &integral) {
                        return append_external_coefficients(std::move(external_coefficients),
                                                            integral->coefficients);
                    });

            std::sort(begin(external_coefficients), end(external_coefficients));
            external_coefficients.erase(
                    std::unique(begin(external_coefficients), end(external_coefficients)),
                    end(external_coefficients));
            return external_coefficients;
        } // namespace

        /// Collect all Functions that are direct inputs to form integrals but also Functions that
        /// are arguments of dependent coefficients
        std::vector<Function>
        gather_functions(const std::vector<std::shared_ptr<IntegralData>> &integrals,
                         const std::vector<ExternalCoefficient> &external_coefficients) {
            auto functions =
                    std::accumulate(begin(integrals), end(integrals), std::vector<Function>{},
                                    append_functions<decltype(*integrals.data())>);
            functions =
                    std::accumulate(begin(external_coefficients), end(external_coefficients),
                                    std::move(functions), append_functions<ExternalCoefficient>);
            std::sort(begin(functions), end(functions));
            functions.erase(std::unique(begin(functions), end(functions)), end(functions));
            return functions;
        }

        /// Collect all Constants that are direct inputs to form integrals but also Functions that
        /// are arguments of dependent coefficients
        std::vector<Constant>
        gather_constants(const std::vector<std::shared_ptr<IntegralData>> &integrals,
                         const std::vector<ExternalCoefficient> &external_coefficients) {
            auto constants =
                    std::accumulate(begin(integrals), end(integrals), std::vector<Constant>{},
                                    append_constants<decltype(*integrals.data())>);
            constants =
                    std::accumulate(begin(external_coefficients), end(external_coefficients),
                                    std::move(constants), append_constants<ExternalCoefficient>);
            std::sort(begin(constants), end(constants));
            constants.erase(std::unique(begin(constants), end(constants)), end(constants));
            return constants;
        }

        /// Extract sub-elements from possibly mixed element
        std::vector<FiniteElement> find_terminal_elements(FiniteElement elmt, std::vector<FiniteElement> elements) {
            if (elmt->block_size() == elmt->num_sub_elements()) {
                elements.push_back(elmt);
            } else {
                for (int i = 0; i < elmt->num_sub_elements(); ++i)
                    elements = find_terminal_elements(elmt->extract_sub_element({i}), elements);
            }
            return std::move(elements);
        }

        /// Extract sub-elements from coefficients of possibly mixed element type
        std::vector<FiniteElement> find_terminal_elements(const std::vector<Coefficient> &coefficients,
                                                          std::vector<FiniteElement> elements) {
            return std::accumulate(begin(coefficients), end(coefficients), std::move(elements),
                                   [&](std::vector<FiniteElement> elements, const Coefficient &coefficient) {
                                       return find_terminal_elements(element(coefficient), elements);
                                   });
        }


        std::function<void(Eigen::Array<scalar_t, Eigen::Dynamic, 1> &, const scalar_t *, int)>
        create_qpoint_coefficient_extractor(const std::vector<Coefficient> &coefficients, int num_eval_points) {

            const std::vector<FiniteElement> terminal_elements = find_terminal_elements(coefficients, {});

            // this allows an optimization since all elements have the same dof ordering
            const bool all_terminals_are_scalar = std::accumulate(begin(terminal_elements), end(terminal_elements),
                                                                  false,
                                                                  [&](bool all_scalar, const FiniteElement &element) {
                                                                      return all_scalar ||
                                                                             element->num_sub_elements() == 1;
                                                                  });

            // optimized version for absence of vector and tensor elements
            if (all_terminals_are_scalar)
                return [=](Eigen::Array<scalar_t, Eigen::Dynamic, 1> &local_coefficients,
                           const scalar_t *coefficients_ptr, int qpoint) {
                    Eigen::Map<
                            const Eigen::Array<scalar_t, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>
                            coefficients_array(coefficients_ptr, local_coefficients.size(), num_eval_points);
                    local_coefficients = coefficients_array.col(qpoint);
                };
            else
                return [=](Eigen::Array<scalar_t, Eigen::Dynamic, 1> &local_coefficients,
                           const scalar_t *coefficients_ptr, int qpoint) {
                    using map_t = Eigen::Map<
                            const Eigen::Array<scalar_t, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>;

                    std::for_each(begin(terminal_elements), end(terminal_elements),
                                  [&, element_offset = 0, qpoint_offset = 0](
                                          const FiniteElement &element) mutable -> void {
                                      map_t element_map{coefficients_ptr + element_offset, num_eval_points,
                                                        element->value_size()};
                                      local_coefficients.middleRows(qpoint_offset,
                                                                    element->value_size()) = element_map.row(qpoint);
                                      element_offset += element->space_dimension();
                                      qpoint_offset += element->value_size();
                                  });
                    // X1 Y1
                    // X2 Y2
                    // X3 Y3
//            // alternative implementation with pointers
//            std::for_each(begin(terminal_elements), end(terminal_elements),
//                          [&, element_ptr = coefficients_ptr, qpoint_ptr = local_coefficients.data()](const FiniteElement &element) mutable -> void {
//                              const scalar_t *element_qpoint_ptr = element_ptr + element->value_size() * qpoint;
//                              std::copy(element_qpoint_ptr, element_qpoint_ptr + element->value_size(), qpoint_ptr);
//                              element_ptr += element->space_dimension();
//                              qpoint_ptr += element->value_size();
//                          }
//            );
                };
        }

        std::function<void(scalar_t *, const Eigen::Array<scalar_t, Eigen::Dynamic, 1> &, int)>
        create_qpoint_result_writer(const FiniteElement &result_element, int num_eval_points) {

            const std::vector<FiniteElement> terminal_elements = find_terminal_elements(result_element, {});

            // this allows an optimization since all elements have the same dof ordering
            const bool all_terminals_are_scalar = std::accumulate(begin(terminal_elements), end(terminal_elements),
                                                                  false,
                                                                  [&](bool all_scalar, const FiniteElement &element) {
                                                                      return all_scalar ||
                                                                             element->num_sub_elements() == 1;
                                                                  });

            // optimized version for absence of vector and tensor elements
            if (all_terminals_are_scalar)
                return [=](scalar_t *target_ptr, const Eigen::Array<scalar_t, Eigen::Dynamic, 1> &local_target,
                           int qpoint) {
                    Eigen::Map<Eigen::Array<scalar_t, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>
                            target_array(target_ptr, result_element->value_size(), num_eval_points);
                    target_array.col(qpoint) = local_target;
                };
            else
                return [=](scalar_t *target_ptr, const Eigen::Array<scalar_t, Eigen::Dynamic, 1> &local_target,
                           int qpoint) {
                    using map_t = Eigen::Map<
                            Eigen::Array<scalar_t, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>;

                    std::for_each(begin(terminal_elements), end(terminal_elements),
                                  [&, element_offset = 0, qpoint_offset = 0](
                                          const FiniteElement &element) mutable -> void {
                                      map_t target_element_map{target_ptr + element_offset, num_eval_points,
                                                               element->value_size()};
                                      target_element_map.row(qpoint) = local_target.middleRows(qpoint_offset,
                                                                                               element->value_size());
                                      element_offset += element->space_dimension();
                                      qpoint_offset += element->value_size();
                                  });

//                // alternative implementation with pointers
//                std::for_each(begin(terminal_elements), end(terminal_elements),
//                              [&, element_ptr = target_ptr, qpoint_ptr = local_target.data()](
//                                      const FiniteElement &element) mutable -> void {
//                                  const scalar_t *element_qpoint_ptr = element_ptr + element->value_size() * qpoint;
//                                  std::copy(qpoint_ptr, qpoint_ptr + element->value_size(), element_qpoint_ptr);
//                                  element_ptr += element->space_dimension();
//                                  qpoint_ptr += element->value_size();
//                              }
//                );
                };
        }

    } // namespace

    ExpressionData::ExpressionData(tabulate_expression_func expression,
                                   FiniteElement finite_element,
                                   std::vector<Coefficient> coefficients,
                                   std::vector<Constant> constants)
            : tabulate_expression{std::move(expression)}, element{std::move(finite_element)},
              coefficients{std::move(coefficients)}, constants{std::move(constants)} {}

    IntegralData::IntegralData(int id, dolfinx::fem::IntegralType type,
                               tabulate_tensor_func tabulate_tensor,
                               std::vector<Coefficient> coefficients,
                               std::vector<Constant> constants,
                               bool needs_permutation_data)
            : id{id}, type{type}, tabulate_tensor{std::move(tabulate_tensor)},
              coefficients{std::move(coefficients)}, constants{std::move(constants)},
              needs_permutation_data{needs_permutation_data} {}

    dolfinx::fem::Form<scalar_t>
    create_form(const std::vector<std::shared_ptr<IntegralData>> &integrals,
                const std::vector<std::shared_ptr<const dolfinx::function::FunctionSpace>>
                &function_spaces) {

        // argument checks
        if (any_null(begin(integrals), end(integrals)))
            throw std::invalid_argument("One of the given integrals is null.");

        if (any_null(begin(function_spaces), end(function_spaces)))
            throw std::invalid_argument("One of the given function spaces is null.");

        // Gather all external coefficients from integrals
        auto external_coefficients = gather_external_coefficients(integrals);

        // Gather all Functions and Constants from integrals and their dependent coefficients
        auto functions = gather_functions(integrals, external_coefficients);
        auto constants = gather_constants(integrals, external_coefficients);

        // Create preprocessor data and, in a second step preprocessors that prepare the form's
        // coefficients and functions for the individual, possibly "custom" integrals.
        const pp_data_container preprocessor_data =
                prepare_pp_data(functions, constants, external_coefficients);
        const pp_container preprocessors = create_preprocessors(preprocessor_data);

        // Create the actual form
        auto form_integrals = create_form_integrals(integrals, preprocessors);
        auto form_coefficients = create_form_coefficients(functions);
        auto form_constants = create_form_constants(constants);
        return dolfinx::fem::Form{function_spaces, form_integrals, form_coefficients,
                                  form_constants};
    }


    ExpressionData create_expression_data(local_function function, FiniteElement finite_element,
                                          std::vector<Coefficient> coefficients,
                                          std::vector<Constant> constants) {
        check_coefficients(coefficients);
        check_constants(constants);

        if (finite_element->family() != "Quadrature")
            throw std::invalid_argument(
                    "Given FiniteElement does not belong to 'Quadrature' family but to '" +
                    finite_element->family() + "'");

        // deduce field sizes for the local function
        const auto local_result_size = finite_element->reference_value_size();

        // deduce number of evaluation_points
        const auto num_eval_points = finite_element->space_dimension() / local_result_size;
        assert(num_eval_points * local_result_size == finite_element->space_dimension());

        // local coefficients size
        const auto local_coefficients_size =
                fields_size(begin(coefficients), end(coefficients)) / num_eval_points;


        for (const auto &coefficient : coefficients) {
            if (const auto &elmt = element(coefficient); elmt->family() != "Quadrature")
                throw std::invalid_argument(
                        "Coefficient does not belong to 'Quadrature' family but to '" + elmt->family() +
                        "'");

            if (const auto &elmt = element(coefficient); elmt->space_dimension() / elmt->value_size() !=
                                                         num_eval_points) {
                std::stringstream msg;
                msg << "Coefficient does not have the same number of evaluation evaluation points as the "
                       "target.\nRequired: "
                    << num_eval_points
                    << "\nActual:" << elmt->space_dimension() / elmt->value_size();
                throw std::invalid_argument(msg.str());
            }
        }

        // Prepare scratch arrays. They will ensure that data is contiguous
        Eigen::Array<scalar_t, Eigen::Dynamic, 1> local_target{local_result_size};
        Eigen::Array<scalar_t, Eigen::Dynamic, 1> local_coefficients{local_coefficients_size};

        const auto qpoint_coefficient_extractor = create_qpoint_coefficient_extractor(coefficients, num_eval_points);
        const auto qpoint_result_writer = create_qpoint_result_writer(finite_element, num_eval_points);

        return {
                [=](scalar_t *_target, const scalar_t *_coefficients, const scalar_t *_constants,
                    const double *_coordinate_dofs) mutable -> void {
                    // Evaluate for each point
                    for (int qpoint = 0; qpoint < num_eval_points; ++qpoint) {
                        qpoint_coefficient_extractor(local_coefficients, _coefficients, qpoint);
                        local_target.setZero();
                        function(local_target.data(), local_coefficients.data(), _constants);
                        qpoint_result_writer(_target, local_target, qpoint);
                    }
                },
                std::move(finite_element), std::move(coefficients), std::move(constants)};
    }

    ExpressionData create_expression_data(tabulate_tensor_func form_a, tabulate_tensor_func form_L,
                                          FiniteElement target_element,
                                          std::vector<Coefficient> coefficients,
                                          std::vector<Constant> constants) {
        check_coefficients(coefficients);
        check_constants(constants);
        const auto target_size = target_element->space_dimension();

        // prepare scratch array
        Eigen::Matrix<scalar_t, Eigen::Dynamic, Eigen::Dynamic> lhs(target_size, target_size);

        return {[=](scalar_t *target, const scalar_t *coefficients, const scalar_t *constants,
                    const double *coordinate_dofs) mutable -> void {
            // the view on the element
            Eigen::Map<Eigen::Array<scalar_t, Eigen::Dynamic, 1>> rhs(target, target_size);

            // compute rhs
            rhs.setZero();
            form_L(target, coefficients, constants, coordinate_dofs, nullptr, nullptr,
                   0 /* unused */);

            // compute lhs
            lhs.setZero();
            form_a(lhs.data(), nullptr, nullptr, coordinate_dofs, nullptr, nullptr,
                   0 /* unused */);

            // "solve" the projection
            rhs /= lhs.diagonal().array();
        },
                std::move(target_element), std::move(coefficients), std::move(constants)};
    }

} // namespace dolfinxcoefficients
