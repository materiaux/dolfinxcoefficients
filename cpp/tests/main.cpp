#define CATCH_CONFIG_RUNNER
#include "catch.hpp"
#include <dolfinx.h>

int main(int argc, char *argv[]) {
    dolfinx::common::SubSystemsManager::init_logging(argc, argv);
    dolfinx::common::SubSystemsManager::init_petsc(argc, argv);
    return Catch::Session().run(argc, argv);
}
