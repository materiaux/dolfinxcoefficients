//
// Created by mrambausek on 04.05.20.
//

#include "catch.hpp"
#include "testdata/testdata.h"
#include <cfloat>
#include <dolfinx.h>
#include <dolfinxcoefficients.h>

TEST_CASE("Form creation c++") {

    using scalar_t = dolfinxcoefficients::scalar_t ;

    using namespace dolfinx;

    // Create mesh and define function space
    std::array<Eigen::Vector3d, 2> pt = {Eigen::Vector3d(0, 0, 0), Eigen::Vector3d(1, 1, 0)};

    auto cmap = fem::create_coordinate_map(create_coordinate_map_testdata);
    auto mesh = std::make_shared<mesh::Mesh>(generation::RectangleMesh::create(
        MPI_COMM_WORLD, pt, {{1, 1}}, cmap, mesh::GhostMode::none));

    // Testspace (delta phi, delta u)
    auto V_0 = fem::create_functionspace(create_functionspace_form_testdata_F, "v_0", mesh);

    // Space for (q)F
    auto V_qF = fem::create_functionspace(create_functionspace_form_testdata_F, "qF", mesh);

    // Space for (q)D
    auto V_qE = fem::create_functionspace(create_functionspace_form_testdata_F, "qE", mesh);

    // Define actual data at quadrature points
    auto F = std::make_shared<function::Function<scalar_t>>(V_qF);
    auto E = std::make_shared<function::Function<scalar_t>>(V_qE);

    // The form to be wrapped
    std::shared_ptr<fem::Form<scalar_t>> L = fem::create_form<scalar_t>(create_form_testdata_F, {V_0});
    auto tabulate_ids = L->integrals().integral_ids(fem::IntegralType::cell);
    assert(size(tabulate_ids) == 1);
    auto tabulate_tensor = L->integrals().get_tabulate_tensor(fem::IntegralType::cell, 0);

    // Form coefficient/constant data
    auto dep_E = V_qE->element();
    auto dep_F = V_qF->element();

    // investigate the comparison of shared_ptr
    REQUIRE(dep_E == V_qE->element());
    auto dep_E2 = std::make_shared<const fem::FiniteElement>(*dep_E);
    REQUIRE(dep_E2 != dep_E);

    // External coefficients
    auto epsilon = std::make_shared<function::Constant<scalar_t>>(15);
    auto qE =
        std::make_shared<dolfinxcoefficients::ExpressionData>(dolfinxcoefficients::create_expression_data(
            [](double *A, const double *w, const double *c) -> void {
                A[0] = 1 * c[0] * w[0];
                A[1] = 2 * c[0] * w[1];
            },
            dep_E, {{E}}, {{epsilon}}));
    auto qF =
        std::make_shared<dolfinxcoefficients::ExpressionData>(dolfinxcoefficients::create_expression_data(
            [](double *A, const double *w, const double *c) -> void {
                A[0] = 1.0 * w[0];
                A[1] = 1.1 * w[1];
                A[2] = 1.2 * w[2];
                A[3] = 1.3 * w[3];
            },
            dep_F, {{F}}, {}));

    std::vector<dolfinxcoefficients::Coefficient> tabulate_coefficients{qE, qF};
    std::vector<dolfinxcoefficients::Constant> tabulate_constants{};
    dolfinxcoefficients::IntegralData integral{-1, fem::IntegralType::cell, tabulate_tensor,
                                            tabulate_coefficients, tabulate_constants, false};

    dolfinxcoefficients::tabulate_expression_func func =
        [](double *A, const double *w, const double *c, const double *cdofs) -> void { A[0] = 0; };
    auto FF = std::make_shared<dolfinxcoefficients::ExpressionData>(
        dolfinxcoefficients::ExpressionData{func, dep_E2, {}, {}});

    // create the new form
    auto new_form =
        dolfinxcoefficients::create_form({{std::make_shared<decltype(integral)>(integral)}}, {{V_0}});

    // check coefficients and constants
    REQUIRE(size(new_form.constants()) == 1);
    REQUIRE(new_form.constants()[0].first == "c_0");
    REQUIRE(new_form.constants()[0].second == epsilon);
    REQUIRE(new_form.coefficients().size() == 2);
    REQUIRE((new_form.coefficients().get(0) == F || new_form.coefficients().get(1) == F));
    REQUIRE((new_form.coefficients().get(0) == E || new_form.coefficients().get(1) == E));
    REQUIRE(new_form.function_space(0) == V_0);

    // interpolate some toy values for numerical evaluatation
    F->interpolate(
        [&](const Eigen::Ref<const Eigen::Array<double, 3, Eigen::Dynamic, Eigen::RowMajor>> &x) {
            Eigen::Array<PetscScalar, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> values(
                F->function_space()->element()->value_size(), x.cols());
            for (Eigen::Index ii = 0; ii < x.cols(); ++ii) {
                const auto pos = x.col(ii);
                auto col = values.col(ii);
                col[0] = 1;
                col[1] = pos(0);
                col[2] = pos(1);
                col[3] = 1 + pos(0) * pos(1);
            }
            return values;
        });

    E->interpolate(
        [&](const Eigen::Ref<const Eigen::Array<double, 3, Eigen::Dynamic, Eigen::RowMajor>> &x) {
            Eigen::Array<PetscScalar, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> values(
                E->function_space()->element()->value_size(), x.cols());
            for (Eigen::Index ii = 0; ii < x.cols(); ++ii) {
                const auto pos = x.col(ii);
                auto col = values.col(ii);
                col[0] = 10 + pos(1);
                col[1] = 17 - pos(0);
            }
            return values;
        });

    // assemble the new form with standard assembler
    Eigen::Matrix<PetscScalar, Eigen::Dynamic, 1> b(V_0->dim());
    dolfinx::fem::assemble_vector<scalar_t>(b, new_form);
    std::cout << b << std::endl;
    // what is the expected result?
}
