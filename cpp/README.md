# dolfinxcoefficients, a framework agnostic dolfin extension for externally evaluated form coefficients 

This package provides a small set of classes for the integration of pointwise function for PDE 
coefficients, i.e. material response functions or constitutive laws, into FEniCS/dolfinx. Corresponding
Python bindings are provided sperately by the Python package `dolfinxcoefficients`.

## Warning

This project is developed against the currently unreleased FEniCS-X ecosystem. It it thus not considered 
as 'stable' but highly experimental and given its early stage also 'incomplete'.


## Installation

Standard CMake installation. Requires `dolfinx` and `Eigen3` version >= 3.3.7.


## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.

